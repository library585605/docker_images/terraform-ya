FROM ubuntu:focal
WORKDIR /root
COPY .terraformrc /root/.terraformrc
RUN apt update &&  apt install -y --no-install-recommends curl wget jq vim unzip git ca-certificates && \
         wget --no-check-certificate https://hashicorp-releases.yandexcloud.net/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip && \
         unzip terraform_1.7.4_linux_amd64.zip && \
         mv terraform /usr/bin/ && \
         wget --no-check-certificate  https://storage.yandexcloud.net/yandexcloud-yc/install.sh && bash install.sh && \
         mv /root/yandex-cloud/bin/yc /usr/bin/ && \
         rm terraform_1.7.4_linux_amd64.zip && \
         curl -LO https://dl.k8s.io/release/v1.31.0/bin/linux/amd64/kubectl && \
         chmod +x ./kubectl && \
         mv ./kubectl /usr/local/bin/kubectl && \
         curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
         chmod 700 get_helm.sh && \
         ./get_helm.sh && \
         rm /root/get_helm.sh && \
         rm /root/install.sh && \
         apt-get purge -y wget curl unzip  && \
         apt-get -y autoremove && \
         rm -Rf /usr/share/doc && rm -Rf /usr/share/man && \
         apt-get clean && \
         rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
CMD ["/bin/bash"]
