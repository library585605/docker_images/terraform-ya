
# Custom docker image Terraform-yandex    

yandexcloud-yc   
terraform_1.7.4   
kubectl v1.31.0    
helm-3   

# Images: ~ 156.91 MiB     
registry.gitlab.com/library585605/docker_images/terraform-ya:latest   
registry.gitlab.com/library585605/docker_images/terraform-ya:v.1.5    

docker pull ganebaldenis/terraform-ya:0.5    

# Run container    
`docker run --rm -it --name terraform-ya registry.gitlab.com/library585605/docker_images/terraform-ya:latest /bin/bash `   
